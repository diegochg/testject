export class OperationsComponent {

  multiply(number1: number, number2: number): number {
    const mult = number1 * number2;
    return mult;
  }

}