import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { OperationsComponent } from './operations/operations.component';
import { ResultsComponent } from './results/results.component';

@NgModule({
  declarations: [
    AppComponent,
    ResultsComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [OperationsComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
