import { ResultsComponent } from './results.component';
import { OperationsComponent } from '../operations/operations.component';

const mockedOperations: jest.Mocked<OperationsComponent> = new OperationsComponent() as any;
let fixture: ResultsComponent = new ResultsComponent(mockedOperations);

describe('ResultsComponent', () => {

  describe('multiplyNumbers', () => {

    it('test instance', () => {
      expect(mockedOperations).toBeTruthy();
      expect(mockedOperations).toBeInstanceOf(OperationsComponent);
    });

    it('test of multiply direct', () => {
      const multiplyNumbersSpy = jest.spyOn(mockedOperations, 'multiply');
      fixture.multiplyNumbers();
      expect(mockedOperations.multiply).toHaveBeenCalled();
      expect(multiplyNumbersSpy).toBeCalledWith(6, 4);
    });

    it('test of multiply numbers parameters', () => {
      jest.spyOn(mockedOperations, 'multiply');
      fixture.multiplyNumbersParameters(10, 3);
      expect(mockedOperations.multiply).toHaveBeenCalled();
      expect(fixture.multiplyNumbersParameters(10, 3)).toBe(30);
      const multMock = jest.fn().mockImplementation(i => i * 5).mockReturnValue(50);
      expect(multMock(5)).toBe(50);
    });

    it('invoke multiply numbers parameters', () => {
      const multiplyNumbersParametersSpy = jest.spyOn(fixture, 'multiplyNumbersParameters');
      fixture.invokeMultiplyNumbersParameters();
      expect(fixture.multiplyNumbersParameters).toHaveBeenCalled();
      expect(multiplyNumbersParametersSpy).toBeCalledWith(8, 7);
    });

  });

});
