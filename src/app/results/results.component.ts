import { Component } from '@angular/core';
import { OperationsComponent } from '../operations/operations.component';

@Component({
  selector: 'app-result',
  templateUrl: 'results.component.html',
})

export class ResultsComponent {

  constructor(private operations: OperationsComponent) { }

  public multiplyNumbers(): number {
    return this.operations.multiply(6, 4);
  }

  public multiplyNumbersParameters(val1: number, val2: number): number {
    return this.operations.multiply(val1, val2);
  }

  public invokeMultiplyNumbersParameters(): number {
    return this.multiplyNumbersParameters(8, 7);
  }

}
